export default {
  // baseUrl:'http://192.168.31.235:8081/api/',
  // wsUrl:'ws://192.168.31.235:8081/api/websocket',
  wsUrl:'wss://myweb.zznzzn.cn/api/websocket',
  baseUrl:'https://myweb.zznzzn.cn/api/',
  timeout:5000,
  fileResources:'uploads/',
  avatarResource:'uploads/avatar/',
  maxSize:1,
}