import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {path: '/', name: 'Home', component: Home},
  {path: '/about', component: () => import("../views/About.vue")},

  {path: '/register', component: () => import('../views/Register.vue')},
  {path: '/login', component: () => import('../views/Login.vue')},
  {path: '/space', component: () => import('../views/Space.vue'),meta: {auth: true}}, // auth true
  {path: '/space/receiving', component: () => import('../views/Receivings.vue'),meta: {auth: true}}, // auth true
  {path: '/item/:itemid', component: () => import('../views/Item.vue'),meta: {auth: true}}, // auth true
  {path: '/user/:uuid', component: () => import('../views/User.vue'),meta: {auth: true}}, // auth true

  {path: '/favourites', component: () => import('../views/Favourite.vue'), meta: {auth: true}},
  {path: '/chat/:uuid', component: () => import('../views/Chat.vue'), meta: {auth: true}},
  {path: '/chat', component: () => import('../views/Chat.vue'), meta: {auth: true}},
  {path: '/orders/:status', component: () => import('../views/Orders.vue'), meta: {auth: true}},


  {path: '/publish/newpub', component: () => import('../views/PublishNew.vue'), meta: {auth: true}},
  {path: '/publish', component: () => import('../views/Publish.vue'), meta: {auth: true}},
  {path: '/publish/:itemid', component: () => import('../views/PublishedItem.vue'),meta: {auth: true}}, // auth true
  {path: '/publish/modify/:itemid', component: () => import('../views/ItemModify.vue'),meta: {auth: true}}, // auth true
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})
router.beforeEach((to, from, next) => {
  // console.log('from :'+from.path+' to : '+to.path)
  if (to.meta.auth) {
    if (store.state.token === '') {
      next({
        path:'/login',
        query:{redirect:to.fullPath}
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
