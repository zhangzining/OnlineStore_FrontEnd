import Axios from 'axios'
import qs from 'qs'
import store from '../store'
import urlconfig from "../urlconfig";

let ajax = Axios.create({
  baseURL: urlconfig.baseUrl,
  timeout: urlconfig.timeout

})
ajax.interceptors.request.use(config => {
    if (config.method === "post") {
      // console.log(config.data instanceof FormData);
      if (!(config.data instanceof FormData)){
        config.data = qs.stringify(config.data)
        // console.log('stringfied');
      }else{
        // console.log("use original formdata")
      }
    }
    return config
  }, error => {
    console.log(error);
  }
)
ajax.defaults.withCredentials = true

ajax.updateAuth = ()=>{
  ajax.defaults.headers.common['Authorization'] = store.state.token
}
ajax.encode=(a) => window.btoa(encodeURIComponent(a))
export default ajax