import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: '',
    nickname: '',
    createtime: '',
    gender: '',
    rank: '',
    status: '',
    username: '',
    uuid: '',
    remember: '',
    avatarpath: '',
    avatarcolor: '',
    email: '',
    description: '',
  },
  mutations: {
    setToken(state, token) {
      state.token = token
    },
    setAvatar(state, data) {
      state.avatarpath = data.filepath
      state.avatarcolor = data.color
    },
    getCachedToken(state) {
      state.token = window.localStorage.getItem("token") ? window.localStorage.getItem("token") : state.token
    },
    init(state, data) {
      state.nickname = data.nickname
      state.username = data.username
      state.gender = data.gender
      state.rank = data.rank
      state.status = data.status
      state.uuid = data.uuid
      state.createtime = data.createtime
      state.token = data.token
      state.avatarcolor = data.avatarcolor
      state.avatarpath = data.avatarpath
      state.description = data.description?data.description:''
      state.email = data.email?data.email:''
    },
    setRemember(state, bool) {
      state.remember = bool
      if (bool) {
        window.localStorage.setItem("token", state.token)
      } else {
        window.localStorage.removeItem("token")
      }
    },
    quit(state) {
      state.nickname = ''
      state.username = ''
      state.gender = ''
      state.rank = ''
      state.status = ''
      state.uuid = ''
      state.createtime = ''
      state.token = ''
      state.email = ''
      state.gender = 0
      state.description = ''
      window.localStorage.removeItem("token")
    }
  },
  getters: {
    hasLogin: (state) => {
      return state.token !== '' && state.username !== ''
    },
    hasProfile:(state)=>{
      return state.email !== '' && state.gender !== 0
    }
  },


  actions: {},
  modules: {}
})
