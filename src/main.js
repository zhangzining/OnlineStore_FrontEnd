import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {BootstrapVue,BootstrapVueIcons} from "bootstrap-vue";
import vRegion from 'v-region'

import 'bootstrap/dist/css/bootstrap.css'

import './css/colors.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


import ajax from "./util/ajax";

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

Vue.use(vRegion)
Vue.prototype.$ajax = ajax

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
